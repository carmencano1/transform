using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
    public Vector3 RotationAxis;
    public float RotationSpeed;

    private void Update() 
    {
        RotationAxis.Clamp(1);
        RotationSpeed*=Time.deltaTime;
        RotationAxis*=RotationSpeed;
        transform.eulerAngles = RotationAxis;
    }
}
